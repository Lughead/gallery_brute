import unittest

import classes.scan
from classes.scan import Gallery
import classes.utils

__author__ = 'jedmitten@alumni.cmu.edu'


class GalleryTests(unittest.TestCase):
    def setUp(self):
        self.lower = 50
        self.upper = 100
        self.domain = 'badkittyphotography.com'

    def test_num_links(self):
        self.assertEqual(25, classes.utils.get_count(self.lower, self.upper, 2))
        self.assertEqual(50, classes.utils.get_count(self.lower, self.upper, 1))


if __name__ == '__main__':
    unittest.main()