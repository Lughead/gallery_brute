# Gallery Brute

This project is a proof-of-concept unauthorized access attack against a Flash web gallery commonly used by photographers. An attacker with no valid credentials is able to access galleries from arbitrary users of the gallery (version unknown). The photographs are then able to be stored out of the gallery and used as the attacker pleases.

## Usage

```python
usage: brute_links.py [-h] --domain DOMAIN -l LOWER_BOUND -u UPPER_BOUND
                      [-s STEP] [-d] [-o OUTPUT_DIR] [-v]

optional arguments:
  -h, --help            show this help message and exit
  --domain DOMAIN       The domain of the gallery
  -l LOWER_BOUND, --lower-bound LOWER_BOUND
                        The smallest number to check for a showId
  -u UPPER_BOUND, --upper-bound UPPER_BOUND
                        The largest number to check for a showId
  -s STEP, --step STEP  The steps to take between lower and upper bounds
  -v, --verbose         Show debugging output

Download Options:
  -d, --download        Whether to download when an interesting gallery has
                        been found
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR
                        The directory to use as a root to save galleries
```