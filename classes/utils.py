import logging

from xml.etree import ElementTree as ET

__author__ = 'jedmitten@alumni.cmu.edu'


log = logging.getLogger(__name__)


def format_url(template, domain, gallery_id, id_len):
    return template.format(domain, str(gallery_id).zfill(id_len))


def get_count(low, high, step):
    return int((high - low) / step)


def get_progress(cur, max):
    return cur / max * 1.0  # force to float


def is_xml(s):
    try:
        _ = ET.fromstring(s)
        return True
    except ET.ParseError:
        return False
