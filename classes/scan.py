import logging

from classes.gallery import Gallery
from classes.download import Downloader
from threading import Thread

__author__ = 'jedmitten@alumni.cmu.edu'


def process_scan(domain, g_id):
    g = Gallery(domain, g_id)
    if g.has_images():
        return g
    return None


class ScanWorker(Thread):
    def __init__(self, queue, do_download=False, outdir='', exit_flag=False):
        Thread.__init__(self)
        self.queue = queue
        self.do_download = do_download
        self.outdir = outdir
        self.exit_flag = exit_flag

        if self.do_download and not self.outdir:
            raise ValueError('Must specify output directory for downloads')

    def run(self):
        while True and not self.exit_flag:
            domain, gallery_id = self.queue.get()
            logging.debug('Pulled {}:{} from the scan queue'.format(domain, gallery_id))
            g = process_scan(domain=domain, g_id=gallery_id)
            if g:
                if self.do_download:
                    Downloader.download(gallery=g, output_dir=self.outdir)
            self.queue.task_done()
