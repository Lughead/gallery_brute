import logging
import urllib.request
import urllib.parse

from xml.etree import ElementTree as ET

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class Constants:
    ID_LEN = 6
    MIN_BYTES = 20000  # the minimum number of bytes to be considered a valid slideshow


class Gallery(object):
    def __init__(self, domain, gallery_id):
        assert isinstance(gallery_id, int)
        self.domain = domain
        self.id = gallery_id
        self.xml_url = build_xml_url(self)
        self.view_url = build_view_url(self)
        self.full_xml = get_gallery_xml(self) or ''
        self.xml_root = get_gallery_root(self.full_xml) or None
        self.name = get_gallery_name(self.full_xml) or ''
        self.image_links = parse_image_urls(self.full_xml)

    def has_images(self):
        return len(self.image_links) > 0


def get_gallery_xml(gallery):
    assert isinstance(gallery, Gallery)
    xml_url = build_xml_url(gallery)
    with urllib.request.urlopen(xml_url) as f:
        response = f.read()
        if is_valid_gallery_xml(response):
            return response
        else:
            return ''


def is_valid_gallery_xml(s_xml):
    try:
        root = ET.fromstring(s_xml)
        return isinstance(root.find('settings/slideshow'), ET.Element)
    except ET.ParseError:
        return False


def get_gallery_name(s_xml):
    root = get_gallery_root(s_xml)
    if root:
        gallery_el = root.find('settings/slideshow')
        gallery_id = gallery_el.attrib['id']
        gallery_name = gallery_el.text
        if gallery_name:
            return '{}_{}'.format(gallery_id, gallery_name)
        else:
            return '{}_'.format(gallery_id)
    else:
        return ''


def get_gallery_root(s_xml):
    assert is_valid_gallery_xml(s_xml)
    root = ET.fromstring(s_xml)
    if not isinstance(root, ET.Element):
        return None

    return root


def parse_image_urls(s_xml):
    """
    Retrieve the response from a url
    :param s_xml: The string representation of gallery xml
    :return:
    """
    image_urls = set([])
    if xml_has_images(s_xml) and is_valid_gallery_xml(s_xml):
        el_root = ET.fromstring(s_xml)
        for image in el_root.iter('image'):
            img_el = image.find('large')
            image_urls.add(img_el.text)

    return image_urls


def xml_has_images(s_xml):
    return len(s_xml) > Constants.MIN_BYTES


def build_xml_url(gallery):
    assert isinstance(gallery, Gallery)
    parts = ('http', gallery.domain, 'viewing/xml_viewing.cfm', 'slideshowID={}'.format(gallery.id), '')
    url = urllib.parse.urlunsplit(parts)
    log.debug('Built XML url: [{}]'.format(url))
    return url


def build_view_url(gallery):
    assert isinstance(gallery, Gallery)
    parts = ('http', gallery.domain, 'viewing/flash/viewing/1/main.swf',
             'web_xml=/viewing/xml_viewing.cfm&web_id={}'.format(gallery.id), '')
    url = urllib.parse.urlunsplit(parts)
    log.debug('Built View url: [{}]'.format(url))
    return url
