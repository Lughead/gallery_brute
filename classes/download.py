import logging
import os
import urllib.request

from classes.gallery import Gallery

__author__ = 'jedmitten@alumni.cmu.edu'


log = logging.getLogger(__name__)


class Downloader(object):
    def __init__(self, gallery, output_dir):
        assert isinstance(gallery, Gallery)
        self.gallery = gallery
        self.output_dir = output_dir

    @staticmethod
    def download(gallery, output_dir):
        log.info('Beginning download of gallery {}:{} to {}'.format(gallery.domain, gallery.id, output_dir))

        gallery_dir = os.path.join(output_dir, gallery.domain)
        if not os.path.isdir(gallery_dir):
            os.makedirs(gallery_dir)

        gallery_dir = os.path.join(gallery_dir, gallery.name)
        if not os.path.isdir(gallery_dir):
            os.mkdir(gallery_dir)
            log.debug('Creating directory [{}]'.format(gallery_dir))

        for image_url in gallery.image_links:
            _, filename = os.path.split(image_url)
            full_dst_path = os.path.join(gallery_dir, filename)
            if not os.path.isfile(full_dst_path):
                log.debug('Downloading [{}] to [{}]'.format(image_url, full_dst_path))
                urllib.request.urlretrieve(image_url, full_dst_path)
        log.info('Download complete of domain: {}, gallery_id: {}'.format(gallery.domain, gallery.id))
