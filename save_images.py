import argparse
import logging
from classes.scan import Gallery

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("save_images")
fh = logging.FileHandler('logs/save_images.log')
log.addHandler(fh)


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', action='store', help='File of urls')
    parser.add_argument('dst_dir', action='store', default='.', help='Directory to store responses')
    parser.add_argument('-v', '--verbose', action='store_true', help='Show debugging output')
    return parser.parse_args()


def main(options):
    if options.verbose:
        log.setLevel(logging.DEBUG)

    with open(options.input_file, 'r') as xml_file:
        xml_urls = xml_file.readlines()

    for url in xml_urls:
        if url != '':
            Gallery.save_gallery(url, options.dst_dir)

    log.debug('Read {} urls form [{}]'.format(len(xml_urls), options.input_file))
    log.info('Writing to directory: [{}]'.format(options.dst_dir))

    log.info('Task completed.')


if __name__ == '__main__':
    args = get_options()
    main(args)
