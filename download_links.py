import urllib.request
import shutil
import argparse
import logging
import os

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("download_links")
fh = logging.FileHandler('logs/downloads.log')
log.addHandler(fh)


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', action='store', help='File of urls')
    parser.add_argument('dst_dir', action='store', default='.', help='Directory to store responses')
    parser.add_argument('-a', '--add-ext', action='store', dest='ext', help='Use flag to add an extension to the file')
    parser.add_argument('-v', '--verbose', action='store_true', help='Show debugging output')
    return parser.parse_args()


def main(options):
    if options.verbose:
        log.setLevel(logging.DEBUG)

    with open(options.input_file, 'r') as url_file:
        urls = url_file.readlines()

    tmp = list()
    for url in urls:
        tmp.append(''.join(url.split()))
    urls = tmp
    
    if not os.path.isdir(options.dst_dir):
        log.debug('Creating destination directory')
        os.makedirs(options.dst_dir)

    log.debug('Read {} urls form [{}]'.format(len(urls), options.input_file))
    log.info('Writing to directory: [{}]'.format(options.dst_dir))
    if options.ext:
        log.debug('Adding extension [{}] to all files'.format(options.ext))

    for url in urls:
        log.debug('Retrieving url: [{}]'.format(url))
        src_fn, headers = urllib.request.urlretrieve(url)
        dst_dir = options.dst_dir
        dst_fn = os.path.split(src_fn)[1]
        if options.ext:
            dst_fn = '{}.{}'.format(dst_fn, options.ext)
        dst_fn = os.path.abspath(os.path.join(dst_dir, dst_fn))
        shutil.move(src_fn, dst_fn)
        log.debug('Saving downloaded file to [{}]'.format(dst_fn))

    log.info('Task completed.')


if __name__ == '__main__':
    args = get_options()
    main(args)
