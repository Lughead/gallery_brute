import argparse
import threading
import time

import classes.scan
import classes.download
import classes.utils

from queue import Queue

import logging
import logging.handlers


exit_flag = False


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', action='store', help='The domain of the gallery', required=True)
    parser.add_argument('-l', '--lower-bound', help='The smallest number to check for a showId', required=True,
                        type=int)
    parser.add_argument('-u', '--upper-bound', help='The largest number to check for a showId', required=True,
                        type=int)
    parser.add_argument('-s', '--step', help='The steps to take between lower and upper bounds', default=1,
                        type=int)
    dl_group = parser.add_argument_group('Download Options')
    dl_group.add_argument('-d', '--download', action='store_true',
                          help='Whether to download when an interesting gallery has been found')
    dl_group.add_argument('-o', '--output-dir', action='store',
                          help='The directory to use as a root to save galleries')
    parser.add_argument('-v', '--verbose', action='store_true', help='Show debugging output')
    return parser.parse_args()


def main(options):
    loglevel = logging.INFO
    if options.verbose:
        loglevel = logging.DEBUG
    logformat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=loglevel, format=logformat)
    log = logging.getLogger("brute_links")
    fh = logging.handlers.RotatingFileHandler('logs/{}_gallery_scanner.log'.format(options.domain),
                                              maxBytes=(2**10)*100, backupCount=10)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(logformat)
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    log.addHandler(fh)
    log.addHandler(ch)

    num_links = classes.utils.get_count(options.lower_bound, options.upper_bound, options.step)
    log.debug('Args: {}'.format(vars(options)))
    log.debug('Checking {} links'.format(num_links))

    validate_options(options)

    scan_queue = Queue()
    log.info('Scanning started...')
    for x in range(5):
        worker = classes.scan.ScanWorker(queue=scan_queue, do_download=options.domain, outdir=options.output_dir,
                                         exit_flag=exit_flag)
        worker.daemon = True
        worker.start()

    for g_id in range(options.lower_bound, options.upper_bound + 1, options.step):
        scan_queue.put((options.domain, g_id))

    scan_queue.join()

    log.info('Task completed.')


def validate_options(options):
    if options.download:
        if not options.output_dir:
            raise ValueError('When downloading, output directory must be specified')


if __name__ == '__main__':
    args = get_options()
    try:
        main(args)
    except KeyboardInterrupt:
        exit_flag = True
        print('exit_flag is set')
        raise
